---
title: "ACE Code Day"
meta_description: "Join us for one of Pleasanton's largest one-day STEM events."
date: 2019-03-26T08:47:11+01:00
draft: false

form_link: "https://docs.google.com/forms/d/e/1FAIpQLSf6UENSXfE2RrNRjkP0h-gWhciE3WhKzN-7Oj_fV6_FrAL0yw/viewform?embedded=true"

itinerary_entries:
  - 8:00 - Check-in
  - 8:00 - Check-in
  - 8:00 - Check-in
  - 8:00 - Check-in
  - 8:00 - Check-in
  - 8:00 - Check-in

workshops:
  - title: ur mom
    level: beginner
    description: aioghewoighwoeiahgowehgowihgiwah oigwhe oiwha goiwehg oiwehg oiewahg woiegh woeiah gioewhg oaiwehgiowe
  - title: ur mom
    level: beginner
    description: aioghewoighwoeiahgowehgowihgiwah oigwhe oiwha goiwehg oiwehg oiewahg woiegh woeiah gioewhg oaiwehgiowe
  - title: ur mom
    level: beginner
    description: aioghewoighwoeiahgowehgowihgiwah oigwhe oiwha goiwehg oiwehg oiewahg woiegh woeiah gioewhg oaiwehgiowe
  - title: ur mom
    level: beginner
    description: aioghewoighwoeiahgowehgowihgiwah oigwhe oiwha goiwehg oiwehg oiewahg woiegh woeiah gioewhg oaiwehgiowe
  - title: ur mom
    level: beginner
    description: aioghewoighwoeiahgowehgowihgiwah oigwhe oiwha goiwehg oiwehg oiewahg woiegh woeiah gioewhg oaiwehgiowe
---

# **Welcome.**

We're ACE Coding—a group of high school students from Amador Valley High School dedicated to spreading the knowledge of programming to students of all ages. Every year, we host ACE Code Day, an event open to anyone (?) where our teachers lead workshops on a variety of programming topics to allow students to further their knowledge in specialized areas of coding. We also host a hackathon for students to participate in and use their new knowledge to build useful projects?.

We would love for you to attend! Sign up below!
